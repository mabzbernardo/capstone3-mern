import { toast } from 'react-toastify';

export const AddedToast = (name) => {
    toast.success('Successfully Added ' + name);
}

export const UpdateToast = (name) => {
    toast.success('Updated ' + name);
}

export const DeleteToast = (name) => {
    toast.error('Deleted ' + name);
}

export const ErrorToast = (message) => {
    toast.error(message);
}

export const SuccessToast = (message) => {
    toast.success(message);
}
