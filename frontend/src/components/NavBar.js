import React, { useState } from 'react';
import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';
import { ListGroup, Navbar } from 'reactstrap';

// Styled Components

const Toggle = styled.div`
    position: fixed;
    top: 20px;
    right: 30px;
    background: pink;
    padding: 10px;
    font-weight:bold;
    border-radius: 4px;
    cursor: pointer;
    display: none;

    @media (max-width: 992px){
        .menuToggle{
            display: block;
        }
    }
`

const Ul = styled.ul`
    ${'' /* top: 20%; */}
    left: 50%;
    width: 65%;
    margin: 0 auto;
    padding: 5px 0;
    background: #74b609;
    display: flex;
    justify-content: center;
    box-shadow: 0 10px 30px rgba(0, 0, 0,.3);
    ${ props => (props.active ? css`` : css``)}
    .li{
        list-style: none;
        text-align:center;
        display: block;
        border-right: 2px solid rgba(0,0,0,.2);
        &:last-child{
        border-right: none;
        }
        ${'' /* #f2ac1f */}
    }
    .li a {
        text-decoration: none;
        padding: 0 35px;
        display: block;
    }
    .li a .icon{
        width:30px;
        height:31px;
        text-align:center;
        overflow:hidden;
        margin: 10px 10px;
    }
    ${'' /* icon */}
    .li a .fa{
        width:100%;
        height:100%;
        line-height: 35px;
        font-size: 25px;
        transition: 0.5s;
        color: #40362A;
        &:last-child{
            color: #fff;
        }
    }

    .li a:hover .icon .fa{
        transform: translateY(-100%);
    }

    .li a .name{
        position: relative;
        height: 18px;
        width: 100%;
        display: block;
        overflow: hidden;
    }

    .li a .name span{
        display: block;
        position: relative;
        color: #fff;
        font-size: 10px;
        line-height: 20px;
        transition: 0.5s;
        font-weight: bold;
    }
    ${'' /* name */}
    .li a .name span:before{
        content: attr(data-text);
        position: absolute;
        top: -100%;
        left: 0;
        width: 100%;
        height: 100%;
        color: #40362a;
    }

    .li a:hover .name span{
        transform: translateY(20px)
    }
    
    @media (max-width: 992px){
        padding:0;
        width:90%;
        display:block;
        overflow: hidden;
        transition: 0.5s;

        .li {
            border-bottom: 1px solid rgba(0,0,0,.2);
            padding: 10px 0;
            display: flex;
        }

        .li:last-child{
            border-bottom: none;
        }

        .li a{
            padding: 0 20px;
        }

        .li a .icon {
            display: inline-block;
            float: left;
            margin: 0 auto;
        }

        .li a .name{
            width: auto;
            display: inline-block;
            margin: 10.5px;
        }
    }
`

// const Container = styled.div`
//     ${ props => (props.active
//         ? css`
//     & .icon .icon2{
//         width:100%;
//         height:100%;
//         line-height: 40px;
//         font-size: 38px;
//         transition: 0.5s;
//         color: #40362A;
//         &:last-child{
//             color: #fff;
//         }
//     }
//     `
//         : css``
//     )}
// `

const NavBar = () => {

    const logout = () => {
        sessionStorage.clear();
        window.location.replace('/login');
    }

    return (
        <>

            <Toggle className="menuToggle">Menu</Toggle>
            <Navbar>
                <Ul>
                    <ListGroup className='li'>
                        <Link to='/home'>
                            <div className="icon">
                                <i className="fa fa-home" aria-hidden="true"></i>
                                <i className="fa fa-home" aria-hidden="true"></i>
                            </div>
                            <div className="name"><span data-text="HOME">HOME</span></div>
                        </Link>
                    </ListGroup>

                    <ListGroup className='li'>
                        <Link
                            to='/about'
                        >
                            <div className="icon">
                                <i className="fa fa-file-text-o" aria-hidden="true"></i>
                                <i className="fa fa-file-text-o" aria-hidden="true"></i>
                            </div>
                            <div className="name"><span data-text="ABOUT">ABOUT</span></div>
                        </Link>
                    </ListGroup>

                    <ListGroup className='li'>
                        <Link to="/mybookings">
                            <div className="icon">
                                <i className="fa fa-bookmark" aria-hidden="true"></i>
                                <i className="fa fa-bookmark" aria-hidden="true"></i>
                            </div>
                            <div className="name"><span data-text="BOOKING">BOOKING</span></div>
                        </Link>
                    </ListGroup>

                    <ListGroup className='li'>

                        <Link to="/bookings">
                            <div className="icon">
                                <i className="fa fa-book" aria-hidden="true"></i>
                                <i className="fa fa-book" aria-hidden="true"></i>
                            </div>
                            <div className="name"><span data-text="BOOK">BOOK</span></div>
                        </Link>

                    </ListGroup>

                    <ListGroup className='li'>
                        <Link to="/schedules">
                            <div className="icon">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </div>
                            <div className="name"><span data-text="SCHEDULE">SCHEDULE</span></div>
                        </Link>
                    </ListGroup>

                    <ListGroup className='li'>
                        <Link to="/fishing">
                            <div className="icon">
                                <i className="fa fa-ship" aria-hidden="true"></i>
                                <i className="fa fa-ship" aria-hidden="true"></i>
                            </div>
                            <div className="name"><span data-text="FISHING">FISHING</span></div>
                        </Link>
                    </ListGroup>

                    <ListGroup className='li'>
                        <Link
                            onClick={logout}
                        >
                            <div className="icon">
                                <i className="fa fa-user" aria-hidden="true"></i>
                                <i className="fa fa-user" aria-hidden="true"></i>
                            </div>
                            <div className="name"><span data-text="LOGOUT">LOGOUT</span></div>
                        </Link>
                    </ListGroup>

                </Ul>
            </Navbar>

        </>
    )
}

export default NavBar;