import React, { useState, useEffect } from 'react';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import styled from 'styled-components';

const Div = styled.div`
    .Range{
        width: 200px;
        font-size: 10px;
    }
`

const ReactDayPicker = ({ dateRange, setDateRange }) => {

    const initialState = { from: null, to: null, enteredTo: null }

    useEffect(() => {
        // setDateRange(initialState);
    }, [])

    const isSelectingFirstDay = (from, to, day) => {
        const isBeforeFirstDay = from && DateUtils.isDayBefore(day, from);
        const isRangeSelected = from && to;
        return !from || isBeforeFirstDay || isRangeSelected;
    }

    const handleDayClick = (day) => {
        const { from, to } = dateRange;
        // console.log(day)
        if (from && to && day >= from && day <= to) {
            handleResetClick();
            return;
        }
        if (isSelectingFirstDay(from, to, day)) {

            setDateRange({
                from: day,
                to: null,
                enteredTo: null,
            });

        } else {

            setDateRange({
                from,
                to: day,
                enteredTo: day,
            });

        }
    }

    const handleDayMouseEnter = (day) => {
        const { from, to } = dateRange;
        if (!isSelectingFirstDay(from, to, day)) {
            setDateRange({
                from,
                to,
                enteredTo: day,
            });
        }
    }

    const handleResetClick = () => {
        setDateRange(initialState);
    }

    const { from, enteredTo } = dateRange;

    return (
        <Div>
            <DayPicker
                className="Range"
                numberOfMonths={2}
                fromMonth={from}
                selectedDays={[from, { from, to: enteredTo }]}
                disabledDays={{ before: from }}
                modifiers={{ start: from, end: enteredTo }}
                onDayClick={handleDayClick}
                onDayMouseEnter={handleDayMouseEnter}
            />
            {/* <div>
                {!dateRange.from && !dateRange.to && 'Please select the first day.'}
                {dateRange.from && !dateRange.to && 'Please select the last day.'}
                {dateRange.from &&
                    dateRange.to &&
                    `Selected from ${dateRange.from.toLocaleDateString()} to
                ${dateRange.to.toLocaleDateString()}`}{' '}
                {dateRange.from && dateRange.to && (
                    <button className="link" onClick={handleResetClick}>
                        Reset
                    </button>
                )}
            </div> */}
        </Div>
    );
}

export default ReactDayPicker;