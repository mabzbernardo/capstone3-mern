import React, { useEffect, useState } from 'react';
import NavBar from '../../components/NavBar';
import { Card, CardHeader, CardBody, Button, Table } from 'reactstrap';
import ScheduleForm from './ScheduleForm'
import ScheduleRow from './ScheduleRow'
import logo from '../../assets/images/logo.png'
import styled from 'styled-components';
import FooterPage from '../../components/FooterPage';


const Main = styled.div`
img{
  height: 12%;
  width: 15%;
  margin-left: 15%;
  margin-top: 10px;
}
`

const Schedules = () => {

    const [showForm, setShowForm] = useState(false);
    const [schedules, setSchedules] = useState([]);
    const [isEditing, setIsEditing] = useState(false);
    const [scheduleToEdit, setScheduleToEdit] = useState({});
    const [addDone, setAddDone] = useState(false);


    useEffect(() => {
        console.log("sched", schedules)


    }, [schedules])


    useEffect(() => {
        fetch('http://localhost:4000/admin/schedules')
            .then(res => res.json())
            .then(res => {
                setSchedules(res);
            })
    }, [addDone])

    const toggleShowForm = () => {
        // if (showForm) setIsEditing(!isEditing)
        setShowForm(!showForm);
        setIsEditing(false)
        setScheduleToEdit({})
    }

    const saveSchedule = (fishpond, dateRange, capacity, amount) => {
        // console.log(capacity, amount);
        const { from, to } = dateRange;
        if (isEditing) {
            let editedFishpond = fishpond;
            let editedstartDate = from;
            let editedendDate = to;
            let editedCapacity = capacity;
            let editedAmount = amount;

            if (fishpond === null) editedFishpond = scheduleToEdit.fishpond;
            // if (dateRange === "") editedDateRange = scheduleToEdit.dateRange;
            if (capacity === 0) editedCapacity = scheduleToEdit.capacity;
            if (amount === 0) editedAmount = scheduleToEdit.amount;
            if (from === null) editedstartDate = scheduleToEdit.startDate;
            if (to === null) editedendDate = scheduleToEdit.endDate;

            const apiOptions = {
                method: "PATCH",
                headers: { "Content-type": "application/json" },
                body: JSON.stringify({
                    id: scheduleToEdit._id,
                    fishpond: editedFishpond,
                    startDate: editedstartDate,
                    endDate: editedendDate,
                    capacity: parseInt(editedCapacity),
                    amount: parseFloat(editedAmount)

                })
            }
            fetch('http://localhost:4000/admin/updateschedule', apiOptions)
                .then(res => res.json())
                .then(res => {
                    let newSchedules = schedules.map(indivSchedule => {
                        if (indivSchedule._id === scheduleToEdit._id) {
                            return res
                        }
                        return indivSchedule;
                    })
                    setSchedules(newSchedules);
                })

        } else {
            const apiOptions = {
                method: "POST",
                headers: { "Content-type": "application/json" },
                body: JSON.stringify({
                    fishpond: fishpond._id,
                    startDate: from,
                    endDate: to,
                    capacity: parseInt(capacity),
                    amount: parseFloat(amount)
                })
            }
            fetch('http://localhost:4000/admin/addschedule', apiOptions)
                .then(res => res.json())
                .then(res => {
                    res.fishpond = fishpond;
                    const newSchedules = [res, ...schedules]
                    setSchedules(newSchedules)

                })
        }
        toggleShowForm();
        setScheduleToEdit({})
        setIsEditing(false);
        setAddDone(!addDone)
    }

    const handleEdit = (indivSchedule) => {
        setScheduleToEdit(indivSchedule)
        setIsEditing(true)
    }

    const deleteSchedule = id => {
        const apiOptions = {
            method: "DELETE",
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({ id })
        }
        fetch('http://localhost:4000/admin/deleteschedule', apiOptions)
            .then(res => res.json())
            .then(() => {
                const newSchedules = schedules.filter(schedule => {
                    return schedule._id != id
                });
                setSchedules(newSchedules);
                // console.log(newSchedules)
            })
    }

    const updateOpenStatus = schedule => {
        let apiOptions = {
            method: "PATCH",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                id: schedule._id,
                isOpen: !schedule.isOpen
            })
        }
        fetch('http://localhost:4000/admin/updateopen', apiOptions)
            .then(res => res.json())
            .then(res => {
                let newSchedules = schedules.map(indivSchedule => {
                    if (schedule._id === indivSchedule._id) {
                        res.fishpond = indivSchedule.fishpond
                        return res
                    }
                    return indivSchedule;
                })
                setSchedules(newSchedules)
            })
    }

    return (
        <>
            <Main>
                <img src={logo} />
                <NavBar />
                <div
                    className='d-flex justify-content-center my-5'
                // style={{ marginLeft: '300px' }}
                >
                    <Card
                        className='col-lg-8'
                        style={{
                            width: '50%',
                            backgroundColor: '#665745'
                        }}
                    >
                        <CardHeader
                            className="text-center"
                            style={{
                                fontSize: '30px',
                                fontWeight: 'bold',
                                backgroundColor: '#665745',
                                color: '#f6a10c'
                            }}
                        >Schedules</CardHeader>
                        <CardHeader>
                            <Button
                                onClick={toggleShowForm}
                                style={{
                                    backgroundColor: '#74b609',
                                    border: 'none'
                                }}
                            >
                                <i class="fa fa-plus" aria-hidden="true"></i> Schedule
                        </Button>
                        </CardHeader>
                        <ScheduleForm
                            showForm={showForm}
                            toggleShowForm={toggleShowForm}
                            saveSchedule={saveSchedule}
                            isEditing={isEditing}
                            scheduleToEdit={scheduleToEdit}
                            setScheduleToEdit={setScheduleToEdit}
                            setIsEditing={setIsEditing}
                        />
                        <CardBody>
                            <Table
                                className='table-stripe text-white'
                            >
                                <thead>
                                    <tr>
                                        <th>Fishpond</th>
                                        <th>Date</th>
                                        <th>Capacity</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {schedules.map(schedule => (
                                        <ScheduleRow
                                            key={schedule._id}
                                            schedule={schedule}
                                            deleteSchedule={deleteSchedule}
                                            handleEdit={handleEdit}
                                            setShowForm={setShowForm}
                                            updateOpenStatus={updateOpenStatus}
                                        />
                                    ))}
                                </tbody>
                            </Table>
                        </CardBody>
                    </Card>
                </div>
            </Main>
            <FooterPage />
        </>
    )
}

export default Schedules;