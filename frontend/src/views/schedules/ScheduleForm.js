import React, { useState, useEffect } from 'react';
import {
    FormGroup,
    ModalHeader,
    ModalBody,
    Modal,
    Input,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    Label,
    DropdownItem,
    Button
} from 'reactstrap';
import DayPicker from '../../components/DayPicker';


const ScheduleForm = ({ toggleShowForm, showForm, saveSchedule, scheduleToEdit, isEditing, setScheduleToEdit, setIsEditing }) => {

    const [fishponds, setFishPonds] = useState([])
    const [open, setOpen] = useState(false)
    const [openDate, setOpenDate] = useState(false)

    //for daypicker
    const [dateRange, setDateRange] = useState({ from: null, to: null, enteredTo: null });

    const [fishpond, setFishPond] = useState({});
    const [capacity, setCapacity] = useState(0);
    const [amount, setAmount] = useState(0)

    useEffect(() => {
        fetch('http://localhost:4000/admin/fishponds')
            .then(res => res.json())
            .then(res => {
                setFishPonds(res)
            })
        if (isEditing) {
            setFishPond(scheduleToEdit.fishpond)
        }
    }, [isEditing, scheduleToEdit])

    // console.log(fishpond);
    return (
        <>
            <Modal
                className='sched'
                isOpen={showForm}
                toggle={() => {
                    toggleShowForm()
                    setFishPond({})
                }}
            >
                <ModalHeader
                    toggle={() => {
                        toggleShowForm()
                        setFishPond({})
                    }}
                >{isEditing ? 'Edit Schedule' : 'Add Schedule'}</ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <Label>Fishpond</Label>
                        <Dropdown
                            isOpen={open}
                            toggle={() => {
                                setOpen(!open)
                                // toggleShowForm()
                            }}
                        >
                            <DropdownToggle caret>{fishpond?.fishpondname ? fishpond.fishpondname : 'Choose'}</DropdownToggle>
                            <DropdownMenu>
                                {fishponds.map(indivFishpond => (
                                    < DropdownItem
                                        key={indivFishpond._id}
                                        onClick={() => setFishPond(indivFishpond)}
                                    >{indivFishpond.fishpondname}</DropdownItem>
                                ))}
                            </DropdownMenu>
                        </Dropdown>
                    </FormGroup>
                    <FormGroup>
                        <Label>Date:</Label>
                        <Dropdown
                            isOpen={openDate}
                            toggle={() => setOpenDate(!openDate)}
                        >
                            <DropdownToggle caret>Choose Date</DropdownToggle>
                            <DropdownMenu>
                                <DayPicker
                                    dateRange={dateRange}
                                    setDateRange={setDateRange}
                                />
                                <Button
                                    type="button"
                                    onClick={() => {
                                        setOpenDate(!openDate)
                                        console.log(dateRange);
                                    }}
                                >OK</Button>
                            </DropdownMenu>

                        </Dropdown>
                    </FormGroup>
                    <FormGroup>
                        <Label>Capacity</Label>
                        <Input
                            placeholder='capacity'
                            type="number"
                            onChange={(e) => setCapacity(e.target.value)}
                            defaultValue={isEditing ? scheduleToEdit.capacity : ""}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label>Amount</Label>
                        <Input
                            placeholder='amount'
                            type="number"
                            onChange={(e) => setAmount(e.target.value)}
                            defaultValue={isEditing ? scheduleToEdit.amount : ""}
                        />
                    </FormGroup>
                    <Button
                        onClick={() => {
                            console.log(dateRange)
                            saveSchedule(fishpond, dateRange, capacity, amount)
                            setFishPond(null)
                            setCapacity(0)
                            setAmount(0)
                            setDateRange({ from: null, to: null, enteredTo: null })
                            toggleShowForm()
                        }}
                    >{isEditing ? "Update" : "Add"}</Button>
                </ModalBody>
            </Modal>
        </>
    )
}

export default ScheduleForm;