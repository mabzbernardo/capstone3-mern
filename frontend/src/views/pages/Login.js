import React, { useState } from 'react';
import loginImg from '../../assets/images/loginImg.svg';
import styled from 'styled-components';
import { FormGroup, Label, Input, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const BaseContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 50px 0;

    .header{
        font-size: 30px;
        font-weight: bold;
    }

    .content{
        display: flex;
        flex-direction: column;
        
        .image{
            width: 300px;
            img{
                width: 100%;
                height: 100%;
            }
        }
        .form{
            ${'' /* margin-top: 30px;
            display: flex;
            flex-direction: column;
            align-items: center; */}
            font-size: 15px;
            .input{
                font-size: 12px;
                background-color: #f3f3f3;
                border: 0;
                border-radius: 4px;
                transition: all 250ms ease-in-out;
                &:focus{
                    outline: none;
                    box-shadow: 0px 0px 12px 0.8px #fca10c;
                }
            }
            .button{
                background-color:#74b609;
                border: none
            }
        }
    }

`

const Login = props => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = async () => {
        const apiOptions = {
            method: "POST",
            headers: { "Content-type": "application/json" },
            body: JSON.stringify({
                email,
                password
            })
        }
        const fetchedData = await fetch('http://localhost:4000/login', apiOptions);
        const data = await fetchedData.json();
        // console.log(data);

        sessionStorage.token = data.token;
        sessionStorage.user = data.user.id;
        sessionStorage.isAdmin = data.user.isAdmin;
        sessionStorage.userName = data.user.firstName + " " + data.user.lastName;
        sessionStorage.email = data.user.email;

        window.location.replace('/home')

    }

    return (
        <>
            <BaseContainer>
                <div className='header'>Login</div>
                <div className='content'>
                    <div className='image'>
                        <img src={loginImg} />
                        <div className='form'>
                            <FormGroup>
                                <Label>Email</Label>
                                <Input
                                    type="text"
                                    className="input"
                                    placeholder="Enter Email"
                                    onChange={(e) => setEmail(e.target.value)}
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label>Password</Label>
                                <Input
                                    type="password"
                                    className="input"
                                    placeholder="Enter Password"
                                    onChange={(e) => setPassword(e.target.value)}
                                />
                            </FormGroup>
                            <FormGroup>
                                <Button
                                    className="button"
                                    block
                                    onClick={handleLogin}
                                    disabled={email === '' || password === '' ? true : false}
                                >Login</Button>
                            </FormGroup>
                            <p>Don't have an account?{' '}
                                <Link
                                    to='/register'
                                    style={{
                                        color: '#f6a10c',
                                        fontWeight: 'bold'
                                    }}
                                >Sign up</Link> Now!</p>
                        </div>
                    </div>

                </div>
            </BaseContainer>
        </>

    )
}

export default Login;