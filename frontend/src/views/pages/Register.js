import React, { useState } from 'react';
import loginImg from '../../assets/images/loginImg.svg';
import styled from 'styled-components';
import { FormGroup, Label, Input, Button } from 'reactstrap'
import { Link } from 'react-router-dom';

const BaseContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    ${'' /* margin: 50px 0; */}
    .header{
        font-size: 30px;
        font-weight: bold;
    }

    .content{
        display: flex;
        flex-direction: column;
        
        .image{
            width: 300px;
            img{
                width: 100%;
                height: 100%;
            }
        }
        .form{
            ${'' /* margin-top: 30px;
            display: flex;
            flex-direction: column;
            align-items: center; */}
            font-size: 15px;
            .input{
                font-size: 12px;
                background-color: #f3f3f3;
                border: 0;
                border-radius: 4px;
                transition: all 250ms ease-in-out;
                &:focus{
                    outline: none;
                    box-shadow: 0px 0px 12px 0.8px #fca10c;
                }
            }
            .button{
                background-color:#74b609;
                border: none
            }
        }
    }

`

const Register = props => {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const handleRegister = async () => {
        const apiOptions = {
            method: "POST",
            headers: { "Content-type": "application/json" },
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                password
            })
        }
        await fetch('http://localhost:4000/register', apiOptions);
        window.location.replace('/login');
    }

    return (
        <>
            <BaseContainer>
                <div className='header'>Register</div>
                <div className='content'>
                    <div className='image'>
                        <img src={loginImg} />
                        <div className='form'>
                            <FormGroup>
                                <Label>First Name</Label>
                                <Input
                                    type="text"
                                    className="input"
                                    placeholder="Enter First Name"
                                    onChange={(e) => setFirstName(e.target.value)}
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label>Last Name</Label>
                                <Input
                                    type="text"
                                    className="input"
                                    placeholder="Enter Last Name"
                                    onChange={(e) => setLastName(e.target.value)}
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label>Email</Label>
                                <Input
                                    type="text"
                                    className="input"
                                    placeholder="Enter Email"
                                    onChange={(e) => setEmail(e.target.value)}
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label>Password</Label>
                                <Input
                                    type="password"
                                    className="input"
                                    placeholder="Enter Password"
                                    onChange={(e) => setPassword(e.target.value)}
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label>Confirm Password</Label>
                                <Input
                                    type="password"
                                    className="input"
                                    placeholder="Enter Password"
                                    onChange={(e) => setConfirmPassword(e.target.value)}
                                />
                                <span className='text-danger'>{confirmPassword !== '' && confirmPassword !== password ? 'Password did not match' : ''}</span>
                            </FormGroup>
                            <FormGroup>
                                <Button
                                    className="button"
                                    block
                                    onClick={handleRegister}
                                    disabled={firstName === '' || lastName === '' || password === '' || password !== confirmPassword ? true : false}
                                >Register</Button>
                            </FormGroup>
                            <p>Already have an account?{' '}
                                <Link
                                    to='/login'
                                    style={{
                                        color: '#f6a10c',
                                        fontWeight: 'bold'
                                    }}
                                >Login</Link></p>
                        </div>
                    </div>
                </div>
            </BaseContainer>
        </>

    )
}

export default Register;