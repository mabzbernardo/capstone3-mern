import React from 'react';
import NavBar from '../../components/NavBar';
import logo from '../../assets/images/logo.png'
import styled from 'styled-components';
import FooterPage from '../../components/FooterPage';
import AboutGallery from './AboutGallery'

const Main = styled.div`
img{
  height: 12%;
  width: 15%;
  margin-left: 15%;
  margin-top: 10px;
}
`

const About = () => {
    return (
        <>
            <Main>
                <img src={logo} />
                <NavBar />
                <h1 className="text-center"
                    style={{
                        fontSize: '30px',
                        fontWeight: 'bold',
                        color: '#f6a10c',
                        margin: '30px'
                    }}
                >About Us</h1>
            </Main>
            <AboutGallery />
            <FooterPage />
        </>
    )
}

export default About;