import React, { useEffect } from 'react';
import styled from 'styled-components'
import NavBar from '../../components/NavBar';
import logo from '../../assets/images/logo.png';
import FooterPage from '../../components/FooterPage';
import { CardHeader, Card, CardImg, CardTitle, CardText, Button } from 'reactstrap';
import abouthome from '../../assets/images/abouthome.jpg';
import events from '../../assets/images/events.jpg';
import bigfish from '../../assets/images/bigfish.jpg'
import { Link } from 'react-router-dom';


const Main = styled.div`
img{
  height: 12%;
  width: 15%;
  margin-left: 15%;
  margin-top: 10px;
}
.welcome{
  color: #f6a10c;
  font-weight: bold;
  margin: 20px;
}
`

const CardDiv = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-around;
  padding: 30px;
  .image{
    height: '500px';
    width: '500px';
    object-fit: cover;
    border: 10px solid #40362a;
  }
  .header{
    text-align: center;
    color: white;
    font-weight: bold;
    font-size: 20px;
    margin: 10px;
  }
  .card{
    box-shadow: 6px 6px 1rem rgba(0,0,0,.5); 
    background-color: #665745;
  }
  .cardText{
    color: white;
  }
  .btn{
    margin: 10px;
  }
`


const Home = props => {

  useEffect(() => {
    if (!sessionStorage.token) {
      window.location.replace('/login');
    }
  })

  return (
    <>
      <Main>
        <img src={logo} />
        <NavBar />
        {/* Tile design */}
        <h1 className="text-center welcome">Welcome {sessionStorage.userName} !</h1>

      </Main>
      <CardDiv>
        <Card className='col-lg-3 card'>
          <CardTitle
            className='header'
          >About Us</CardTitle>
          <CardImg
            className='image'
            src={abouthome}
          />
          <CardText
            className='cardText'
          ><br />Tagalag Fishing Village is known to be the Best Kept Secret of Valenzuela City.</CardText>
          <Button
            className='btn'
            style={{
              backgroundColor: '#f6a10c'
            }}
          ><Link
            to='/about'
            style={{
              color: 'white'
            }}
          >View</Link></Button>
        </Card>

        <Card className='col-lg-3 card'>
          <CardTitle
            className='header'
          >Catch Big Fish</CardTitle>
          <CardImg
            className='image'
            src={bigfish}
          />
          <CardText
            className='cardText'
          ><br />Beautiful sunset, fresh air, and catching of fish can be experienced along Tagalag Road.</CardText>
          <Button
            className='btn'
            style={{
              backgroundColor: '#f6a10c'
            }}
          ><Link
            to='/about'
            style={{
              color: 'white'
            }}
          >View</Link></Button>
        </Card>

        <Card className='col-lg-3 card'>
          <CardTitle
            className='header'
          >Events</CardTitle>
          <CardImg
            className='image'
            src={events}
          />
          <CardText
            className='cardText'
          >It also has the Tagalag Eco Park where people visit to jog, bike, and relax with their loved ones.</CardText>
          <Button
            className='btn'
            style={{
              backgroundColor: '#f6a10c'
            }}
          ><Link
            to='/about'
            style={{
              color: 'white'
            }}
          >View</Link></Button>
        </Card>

      </CardDiv>
      <FooterPage />
    </>
  )
}

export default Home;