import React, { useState } from 'react';
import { Button } from 'reactstrap';
import PaymentForm from './PaymentForm';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import moment from 'moment';
import DownloadTicket from './DownloadTicket';


const stripePromise = loadStripe('pk_test_UiHV5PNiBC4RKTo0IYpPk8AG00HufIEYeh')

const MyBookingRow = ({ booking, cancelBooking, updateBooking }) => {


    const [showForm, setShowForm] = useState(false);

    return (
        <>
            <tr>
                <td>{booking.schedule.fishpond.fishpondname}</td>
                <td>{booking.user.firstName} {booking.user.lastName}</td>
                <td>{moment(booking.schedule.startDate).format('LL')}-{moment(booking.schedule.endDate).format('LL')}</td>
                <td>{booking.schedule.amount}</td>
                <td>{booking.status}</td>
                <td>{booking.payment}</td>
                <td>
                    <Button
                        color='danger'
                        disabled={booking.status === 'Pending' ? false : true}
                        onClick={() => cancelBooking(booking)}
                    ><i class="fa fa-ban" aria-hidden="true"></i></Button>
                </td>
                <td>
                    <Button
                        style={{
                            backgroundColor: '#f6a10c'
                        }}
                        onClick={() => setShowForm(true)}
                        disabled={booking.payment === 'Stripe' || booking.status === 'Cancelled'}
                    ><i class="fa fa-cc-stripe" aria-hidden="true"></i></Button>
                    <Elements
                        stripe={stripePromise}
                    >
                        <PaymentForm
                            setShowForm={setShowForm}
                            showForm={showForm}
                            amount={booking.amount}
                            bookingId={booking._id}
                            updateBooking={updateBooking}
                        />
                    </Elements>
                </td>
                <td>{booking.status === 'Paid' &&
                    <DownloadTicket
                        booking={booking} />}</td>
            </tr>
        </>
    )
}

export default MyBookingRow;