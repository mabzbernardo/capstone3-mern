import React, { useState, useEffect } from 'react';
import NavBar from '../../components/NavBar';
import { Card, CardBody, Table, CardHeader, Button } from 'reactstrap';
import MyBookingRow from './MyBookingRow';
import styled from 'styled-components';
import logo from '../../assets/images/logo.png';
import FooterPage from '../../components/FooterPage';
import Loader from "../../components/Loader";
import { CSVLink } from 'react-csv';

const Main = styled.div`
img{
  height: 12%;
  width: 15%;
  margin-left: 15%;
  margin-top: 10px;
}
`

const MyBookingsCard = styled.div`
display: flex;
justify-content: center;
align-items: center;
h1{
        color: #fca10c;
        background-color: #665745;
    }
    .csvBtn{
        background-color: #f6a10c;
        border: none;
    }
`

const MyBookings = () => {

    const [bookings, setBookings] = useState([]);
    const [isLoading, setIsLoading] = useState(false)
    const [allBookingData, setAllBookingData] = useState([])

    useEffect(() => {
        fetch('http://localhost:4000/admin/bookings/' + sessionStorage.user)
            .then(res => res.json())
            .then(res => {
                setBookings(res)
            })
    }, [])

    if (isLoading) return <Loader />

    const bookingData = [
        ['ID', 'Fishpond', 'Owner', 'Start Date', 'End Date', 'Price', 'Reservation Status', 'Payment Status']
    ]

    allBookingData.forEach(indivBooking => {
        let indivBookingData = []

        indivBookingData.push(indivBooking._id);
        indivBookingData.push(indivBooking.fishpond.fishpondname);
        indivBookingData.push(indivBooking.user.firstName);
        indivBookingData.push(indivBooking.schedule.startDate);
        indivBookingData.push(indivBooking.schedule.endDate);
        indivBookingData.push(indivBooking.amount);
        indivBookingData.push(indivBooking.status);
        indivBookingData.push(indivBooking.payment);

        bookingData.push(indivBookingData);
    })

    const cancelBooking = booking => {
        const apiOptions = {
            method: "PATCH",
            headers: { "Content-type": "application/json" },
            body: JSON.stringify({
                id: booking._id,
                schedule: booking.schedule._id
            })
        }
        fetch('http://localhost:4000/admin/cancelbooking', apiOptions)
            .then(res => res.json())
            .then(res => {
                console.log(res)
                let newBookings = bookings.map(indivBooking => {
                    if (indivBooking._id === res._id) {
                        indivBooking.status = res.status;
                        indivBooking.payment = res.payment
                    }
                    return indivBooking;
                })
                setBookings(newBookings)
            })
    }

    const updateBooking = id => {
        // console.log(id)
        const newBookings = bookings.map(indivBooking => {
            if (indivBooking._id === id) {
                indivBooking.status = 'Paid';
                indivBooking.payment = 'Stripe';
            }
            return indivBooking;
        })
        setBookings(newBookings)
    }

    return (
        <>
            <Main>
                <img src={logo} />
                <NavBar />
                <MyBookingsCard>
                    <div className="myCard col-lg-10 my-5">
                        <Card
                            style={{
                                backgroundColor: "#665745"
                            }}
                        >
                            <CardHeader
                                className="text-center"
                                style={{
                                    fontSize: '30px',
                                    fontWeight: 'bold',
                                    backgroundColor: '#665745',
                                    color: '#f6a10c',
                                }}
                            >My Bookings</CardHeader>
                            <CardHeader>
                                <Button
                                    className='csvBtn'
                                >
                                    <CSVLink
                                        style={{
                                            color: 'white'
                                        }}
                                        data={bookingData}
                                        filename={'fishpond_reports on' + new Date() + '.csv'}
                                    >
                                        <i class="fa fa-download" aria-hidden="true"></i> CSV</CSVLink></Button>
                            </CardHeader>
                            <CardBody
                            >
                                <Table
                                    style={{
                                        color: 'white'
                                    }}
                                >
                                    <thead>
                                        <tr>
                                            <th>Fishpond</th>
                                            <th>Owner</th>
                                            <th>Date</th>
                                            <th>Price</th>
                                            <th>Reservation Status</th>
                                            <th>Payment Status</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {bookings.map(booking => (
                                            <MyBookingRow
                                                key={booking._id}
                                                booking={booking}
                                                cancelBooking={cancelBooking}
                                                updateBooking={updateBooking}
                                            />
                                        ))}
                                    </tbody>

                                </Table>
                            </CardBody>
                        </Card>
                    </div>
                </MyBookingsCard>
            </Main>
            <FooterPage />
        </>
    )
}

export default MyBookings;