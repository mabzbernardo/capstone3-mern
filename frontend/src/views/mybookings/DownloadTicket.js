import React, { useState, useEffect } from 'react';
import { Document, Page, View, Text, StyleSheet, PDFDownloadLink, Image } from '@react-pdf/renderer';
import { Button } from 'reactstrap';
import moment from 'moment';
// import logo from '../../assets/images/logo.png';

const styles = StyleSheet.create({
    header: {
        height: '200px',
        width: '100%',
        textAlign: 'center',
        backgroundColor: '#665745'
    },
    brandName: {
        fontSize: 40,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#f6a10c'
    },
    detailsContainer: {
        display: 'flex',
        justifyContent: 'center',
        margin: '20px'
    },
    details: {
        margin: '10% 20%'
    },
    detailText: {
        color: 'darkgray',
        textAlign: 'center'
    }
})

const Ticket = ({ booking }) => {
    return (
        <Document>
            <Page size='A4'>
                <View
                    style={styles.header}
                >
                    <Text style={{
                        margin: '50px',
                        fontSize: 25,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        color: '#f6a10c'
                    }}
                    >TAGALAG FISHING VILLAGE</Text>
                </View>
                <View
                    style={styles.detailsContainer}
                >
                    <View style={styles.details}>
                        <Text style={styles.brandName}>ADMIT ONE</Text>
                        <Text style={styles.detailText}>Fishpond Name: {booking.schedule.fishpond.fishpondname}</Text>
                        <Text style={styles.detailText}>Owner: {booking.user.firstName} {booking.user.lastName}</Text>
                        <Text style={styles.detailText}>Date: {moment(booking.schedule.startDate).format('LL')} - {moment(booking.schedule.endDate).format('LL')}</Text>
                        <Text style={styles.detailText}>Status: {booking.status}</Text>
                        <Text style={styles.detailText}>Paid by: {sessionStorage.userName}</Text>
                    </View>
                    <View style={styles.details}>
                        <Text style={styles.detailText}>This ticket is non-refundable</Text>
                    </View>
                    <View>
                        {/* <Image src={logo} /> */}
                    </View>
                </View>
            </Page>
        </Document>
    )
};

const DownloadTicket = ({ booking }) => {

    const [open, setOpen] = useState(false);

    useEffect(() => {
        setOpen(false);
        setOpen(true)
        return () => setOpen(false)
    })

    return (
        <Button
        >
            {
                open && <PDFDownloadLink
                    document={<Ticket booking={booking} />}
                    fileName={"Ticket#" + Date.now() + ".pdf"}
                >
                    {({ loading }) => loading ? "Loading..." : 'Download Ticket'}
                </PDFDownloadLink>
            }
        </Button>
    )
}

export default DownloadTicket;

