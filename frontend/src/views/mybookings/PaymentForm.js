import React from 'react';
import { Modal, FormGroup, ModalHeader, ModalBody, Button } from 'reactstrap';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { SuccessToast, ErrorToast } from '../../components/Toast';

const PaymentForm = ({ setShowForm, showForm, amount, bookingId, updateBooking }) => {

    const stripe = useStripe();
    const elements = useElements();

    const saveCharge = async () => {
        const cardElement = elements.getElement(CardElement);

        const { token } = await stripe.createToken(cardElement);

        const apiOptions = {
            method: "POST",
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({
                token: token.id,
                email: sessionStorage.email,
                amount: amount * 100,
                id: bookingId
            })
        }

        const paymentResponse = await fetch('http://localhost:4000/charge', apiOptions);
        console.log(paymentResponse)

        if (paymentResponse.ok) {
            SuccessToast('Successfully paid');
            updateBooking(bookingId);
        } else {
            ErrorToast('An error occured. Please try again.');
        }
        setShowForm(false);
    }

    return (
        <>
            <Modal
                isOpen={showForm}
                toggle={() => setShowForm(false)}
            >
                <ModalHeader
                    toggle={() => setShowForm(false)}
                >Payment</ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <CardElement />
                        <Button
                            color='success'
                            disabled={!stripe}
                            onClick={saveCharge}
                        >Submit Payment</Button>
                    </FormGroup>
                </ModalBody>
            </Modal>
        </>
    )
}

export default PaymentForm;