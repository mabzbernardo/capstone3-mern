import React, { useState, useEffect } from 'react';
import NavBar from '../../components/NavBar';
import BookingInfo from './BookingInfo';
import { AddedToast } from '../../components/Toast';
import logo from '../../assets/images/logo.png'
import styled from 'styled-components';
import FooterPage from '../../components/FooterPage';

const Main = styled.div`
img{
  height: 12%;
  width: 15%;
  margin-left: 15%;
  margin-top: 10px;
}

.bookingImg{
    height: 250px;
    width:250px;
    margin-left: auto;
}

.bookingInfo{
    margin: 5% 2%;
}
`

const Content = styled.div`
${'' /* margin-left: 10%; */}
margin: 5% 25%;
width: 100%;
${'' /* margin-bottom: 1em; */}
`

const Bookings = () => {

    const [schedules, setSchedules] = useState([]);

    useEffect(() => {
        fetch('http://localhost:4000/admin/schedules')
            .then(res => res.json())
            .then(res => {
                const newSchedules = res.filter(indivSchedule => {
                    return indivSchedule.isOpen
                });
                // console.log(res);
                setSchedules(newSchedules);
            })
    }, [])

    const saveBooking = (schedule) => {
        const apiOptions = {
            method: "POST",
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({
                schedule: schedule._id,
                user: sessionStorage.user,
                fishpond: schedule.fishpond._id,
                amount: schedule.amount,
                payment: "Pending",
            })
        }
        fetch('http://localhost:4000/admin/addbooking', apiOptions)
            .then(res => res.json())
            .then(res => {
                AddedToast('fishpond');
                let newSchedules = schedules.map(indivSchedule => {
                    if (indivSchedule._id === res._id) {
                        indivSchedule.seats = res.seats
                    }
                    return indivSchedule;
                });
                setSchedules(newSchedules);
            });
    }

    return (
        <>
            <Main>
                <img src={logo} />
                <NavBar />
                <h1 className="text-center"
                    style={{
                        fontSize: '30px',
                        fontWeight: 'bold',
                        color: '#f6a10c',
                        margin: '30px'
                    }}
                >Book Now</h1>
                <Content>
                    {schedules.map(schedule => (
                        <BookingInfo
                            key={schedule._id}
                            schedule={schedule}
                            saveBooking={saveBooking}
                        />
                    ))}
                </Content>
            </Main>
            <FooterPage />
        </>
    )
}

export default Bookings;