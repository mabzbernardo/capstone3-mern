import React from 'react';
import styled from 'styled-components';
import { Button } from 'reactstrap';


const Container = styled.div`
    display: flex;
    border: 2px solid #f6a10c;
    width: 50%;
    ${'' /* position: absolute; */}
    left: 34%;
    background-color: #665745;
    color: #fff;
`

const Image = styled.div`
    img{
        height: 400px;
        width: 400px;
        margin: 5%;
        border: 10px solid #40362a;
    }
`

const BookingInfo = ({ schedule, saveBooking }) => {
    return (
        <>
            <Container>
                <Image>
                    <img
                        className="bookingImg"
                        src={"http://localhost:4000/" + schedule.fishpond.image}
                    // src="https://picsum.photos/200"
                    />
                </Image>
                <div className="bookingInfo">
                    <h3>Fishpond: {schedule.fishpond.fishpondname}</h3>
                    <h6>Owner: {schedule.fishpond.user.firstName} {schedule.fishpond.user.lastName} </h6>

                    <h6>Description: {schedule.fishpond.description}</h6>
                    <h6>Capacity: {schedule.capacity}</h6>
                    <h6>Amount: {schedule.amount}</h6>
                    <Button
                        style={{
                            width: '50%',
                            backgroundColor: '#f6a10c'
                        }}
                        onClick={() => saveBooking(schedule)}
                        disabled={schedule.capacity === 0 ? true : false}
                    >Book Now</Button>
                </div>
            </Container>
        </>
    )
}

export default BookingInfo;