import React, { useState, useEffect } from 'react';
import NavBar from '../../components/NavBar';
import { Button, CardHeader } from 'reactstrap';
import FishingInfo from './FishingInfo'
import FishingForm from './FishingForm';
import DayPicker from '../../components/DayPicker';
import { Card, CardBody, Table } from 'reactstrap';
import { CSVLink } from 'react-csv';
import Loader from "../../components/Loader";
import FooterPage from '../../components/FooterPage';
import styled from 'styled-components'
import logo from '../../assets/images/logo.png';

const Main = styled.div`
    img{
    height: 12%;
    width: 15%;
    margin-left: 15%;
    margin-top: 10px;
    }
    .card{
        background-color: '#665745' !important;
    }
    h1{
        text-color: '#f6a10c';
    }
    .csvBtn{
        background-color: #f6a10c;
        border: none;
    }
    .addBtn{
        background-color: #74b609;
    }
`

const Fishing = () => {

    const [isLoading, setIsLoading] = useState(false)
    const [fishponds, setFishponds] = useState([])
    const [showForm, setShowForm] = useState(false);
    const [addDone, setAddDone] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [fishpondToEdit, setFishpondToEdit] = useState({})
    const [allFishing, setAllFishing] = useState([])

    useEffect(() => {
        console.log(sessionStorage.user)
        fetch("http://localhost:4000/admin/fishponds")
            .then(res => res.json())
            .then(res => {
                setFishponds(res)
                setAllFishing(res);
                // console.log(res)
            })
    }, [addDone])

    if (isLoading) return <Loader />

    const fishingData = [
        ['ID', 'Fishpond', 'Owner', 'Description', 'Capacity']
    ]

    allFishing.forEach(indivFishing => {
        let indivFishingData = [];

        indivFishingData.push(indivFishing._id);
        indivFishingData.push(indivFishing.fishpondname);
        indivFishingData.push(indivFishing.user.firstName);
        indivFishingData.push(indivFishing.description);
        indivFishingData.push(indivFishing.capacity);

        fishingData.push(indivFishingData);
    })

    const saveFishPond = (fishpondname, image, description, capacity) => {
        console.log(fishpondname, image, description, capacity, isEditing)

        if (isEditing) {
            let editingPondname = fishpondname;
            let editingImage = image;
            let editingDescription = description;
            let editingCapacity = capacity;

            if (fishpondname === "") editingPondname = fishpondToEdit.fishpondname;
            if (image === "") editingImage = fishpondToEdit.image;
            if (description === "") editingDescription = fishpondToEdit.description;
            if (capacity === "") editingCapacity = fishpondToEdit.capacity;

            const apiOptions = {
                method: "PATCH",
                headers: { "Content-type": "application/json" },
                body: JSON.stringify({
                    id: fishpondToEdit._id,
                    fishpondname: editingPondname,
                    image: editingImage,
                    description: editingDescription,
                    capacity: editingCapacity
                })
            }
            // console.log(apiOptions)
            fetch('http://localhost:4000/admin/updatefishpond', apiOptions)
                .then(res => res.json())
                .then(res => {
                    let newFishPonds = fishponds.map(indivFishPond => {
                        if (indivFishPond._id === fishpondToEdit._id) {
                            return res
                        }
                        return indivFishPond;
                    })
                    setFishponds(newFishPonds);
                })
        } else {
            let apiOptions = {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    fishpondname,
                    user: sessionStorage.user,
                    image,
                    description,
                    capacity: parseInt(capacity)
                })
            }


            fetch('http://localhost:4000/admin/addfishpond', apiOptions)
                .then(res => res.json())
                .then(res => {
                    console.log(res);
                    let user = res.user
                    setFishponds([res, ...fishponds])
                    // setFishponds(prev => res);
                })
        }

        setShowForm(false);
        setAddDone(!addDone);
        setFishpondToEdit({});
        setIsEditing(false)
        setIsLoading(false)
    }

    const handleEdit = (indivFishPond) => {
        setFishpondToEdit(indivFishPond)
        setIsEditing(true)
        console.log(fishpondToEdit)
    }

    const deleteFishPond = (id) => {
        let apiOptions = {
            method: "DELETE",
            headers: { "Content-type": "application/json" },
            body: JSON.stringify({ id }),
        };

        fetch('http://localhost:4000/admin/deletefishpond', apiOptions)
            .then(res => res.json())
            .then(res => {
                const newFishPonds = fishponds.filter((indivFishPond) => {
                    return indivFishPond._id !== id;
                });
                setFishponds(newFishPonds);
            })
    }

    const toggleShowForm = () => {
        setShowForm(!showForm)
        setIsEditing(false)
        setFishpondToEdit({})
    }

    return (
        <>
            <Main>
                <img src={logo} />
                <NavBar />

                {/* <DayPicker /> */}
                <FishingForm
                    showForm={showForm}
                    toggleShowForm={toggleShowForm}
                    saveFishPond={saveFishPond}
                    isEditing={isEditing}
                    fishpondToEdit={fishpondToEdit}
                />
                <div>
                    <Card
                        className='card col-lg-10 offset-1 my-5'
                        style={{
                            backgroundColor: '#665745'
                        }}
                    >
                        <CardHeader
                            className="text-center"
                            style={{
                                fontSize: '30px',
                                fontWeight: 'bold',
                                backgroundColor: '#665745',
                                color: '#f6a10c',

                            }}
                        >Fishing</CardHeader>
                        <CardHeader>
                            <Button
                                style={{
                                    border: 'none'
                                }}
                                className='addBtn'
                                onClick={() => setShowForm(!showForm)}
                            ><i class="fa fa-plus" aria-hidden="true"></i> Fishpond</Button>
                            <Button
                                className='csvBtn mx-2'
                            >
                                <CSVLink
                                    style={{
                                        color: 'white'
                                    }}
                                    data={fishingData}
                                    filename={'fishpond_reports on' + new Date() + '.csv'}
                                ><i class="fa fa-download" aria-hidden="true"></i> CSV</CSVLink>
                            </Button>
                        </CardHeader>
                        <CardBody>
                            <Table
                                className='text-white'
                            >
                                <thead>
                                    <tr>
                                        <th>Fishpond</th>
                                        <th>Owner</th>
                                        <th>Description</th>
                                        <th>Capacity</th>
                                        <th>Image</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                {fishponds.map(indivFishPond => (
                                    <FishingInfo
                                        key={indivFishPond._id}
                                        indivFishPond={indivFishPond}
                                        deleteFishPond={deleteFishPond}
                                        setShowForm={setShowForm}
                                        handleEdit={handleEdit}
                                    />
                                ))}
                            </Table>
                        </CardBody>
                    </Card>
                </div>
            </Main>
            <FooterPage />
        </>
    )
}

export default Fishing;