import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, FormGroup, Label, Input, Button } from 'reactstrap';
import { ErrorToast } from "../../components/Toast";

const FishingForm = ({ showForm, toggleShowForm, saveFishPond, isEditing, fishpondToEdit }) => {

    const [pondName, setPondName] = useState('');
    // const [ownersName, setOwnersName] = useState('')
    const [image, setImage] = useState(null)
    const [description, setDescription] = useState('')
    const [capacity, setCapacity] = useState('')

    const handleSaveFishPond = async () => {
        let { imageUrl } = await uploadMainImage(image);
        saveFishPond(pondName, imageUrl, description, capacity);
        setPondName("");
        setImage("");
        setDescription("");
        setCapacity("");
        console.log(imageUrl)
    }

    const selectMainImage = e => {
        // console.log(e.target.files)

        let image = e.target.files[0]
        // console.log(image);
        if (image.name.match(/\.(jpg|JPG|JPEG|jpeg|png|PNG|gif|GIF)$/)) {
            setImage(image);
            // console.log(image);
        } else {
            ErrorToast('Invalid Image. Please try again.');
        }
    }

    const uploadMainImage = async (imageToSave) => {
        const data = new FormData();
        data.append('image', image, imageToSave.name);

        const imageData = await fetch('http://localhost:4000/upload', {
            method: "POST",
            body: data
        })

        const imageUrl = await imageData.json();
        // console.log(imageUrl);
        return imageUrl;
    }

    return (
        <>
            <Modal
                isOpen={showForm}
                toggle={toggleShowForm}
            >
                <ModalHeader
                    toggle={toggleShowForm}
                >{isEditing ? "Edit Location" : "Add Location"}</ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <Label>Fishpond Name:</Label>
                        <Input
                            placeholder='Fishpond Name'
                            onChange={e => setPondName(e.target.value)}
                            defaultValue={isEditing ? fishpondToEdit.fishpondname : ""}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label>Image</Label>
                        <Input
                            placeholder="Image file"
                            type="file"
                            onChange={selectMainImage}
                        // defaultValue={isEditing ? fishpondToEdit.image : ""}
                        // type='text'
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label>Description: </Label>
                        <Input
                            placeholder="Description"
                            type='textarea'
                            onChange={e => setDescription(e.target.value)}
                            defaultValue={isEditing ? fishpondToEdit.description : ""}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label>Capacity: </Label>
                        <Input
                            placeholder="Capacity"
                            type='number'
                            onChange={e => setCapacity(e.target.value)}
                            defaultValue={isEditing ? fishpondToEdit.capacity : ""}
                        />
                    </FormGroup>
                    <Button
                        // onClick={() => saveFishPond(pondName, image, description, capacity)}
                        onClick={() => {
                            handleSaveFishPond(pondName, image, description, capacity);
                            setPondName("");
                            setImage("");
                            setDescription("");
                            setCapacity("");
                        }}
                    >{isEditing ? "Update" : "Save"}</Button>
                </ModalBody>
            </Modal>
        </>
    )
}

export default FishingForm;