const express = require('express');

const PaymentRouter = express.Router();

const stripe = require('stripe')('sk_test_9XuhfxlCvUON6I35RkepsOhy00lZXqVZzR');
const BookingModel = require('../models/Booking');

PaymentRouter.post('/charge', async (req, res) => {
    // console.log(req.body)
    try {
        const customer = await stripe.customers.create({
            email: req.body.email,
            description: "Payment for Fishpond",
            source: "tok_visa"
        });
        let chargeResponse = await stripe.charges.create({
            amount: req.body.amount,
            currency: 'php',
            source: "tok_visa",
            description: "Payment for Fishpond"
        });

        await BookingModel.findByIdAndUpdate(req.body.id, { status: "Paid", payment: 'Stripe' });

        res.send(chargeResponse);
    } catch (e) {
        // console.log(e);
        res.status(401).send('Bad Request. Please try again.')
    }
})

module.exports = PaymentRouter;