const express = require('express');

const BookingRouter = express.Router();

const BookingModel = require('../models/Booking');

const ScheduleModel = require('../models/Schedule')


//add booking
BookingRouter.post('/addbooking', async (req, res) => {
    // console.log(req.body)
    try {
        let booking = BookingModel({
            schedule: req.body.schedule,
            user: req.body.user,
            fishpond: req.body.fishpond,
            payment: req.body.payment,
            amount: req.body.amount
        });
        booking = await booking.save();
        let schedule = await ScheduleModel.findById(req.body.schedule);
        let updatedSchedule = await ScheduleModel.findByIdAndUpdate(req.body.schedule, { capacity: schedule.capacity - 1 }, { new: true })
        res.send(updatedSchedule)
    } catch (e) {
        res.status(401).json('Bad request. Please try again.')
    }
});

//get booking
BookingRouter.get('/bookings', async (req, res) => {

    try {
        // let bookings = await BookingModel.find().populate(['schedule', 'user', 'fishpond']);
        let bookings = await BookingModel.find().populate({
            path: 'schedule',
            populate: ['fishpond']
        }).populate('user')

        res.send(bookings)
        // console.log(bookings)
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})

//get booking by user id
BookingRouter.get('/bookings/:id', async (req, res) => {
    try {
        // let bookings = await BookingModel.find().populate(['schedule', 'user', 'fishpond']);
        let bookings = await BookingModel.find({ user: req.params.id }).populate({
            path: 'schedule',
            populate: ['fishpond']
        }).populate('user')
        // console.log(bookings)
        res.send(bookings)
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})

//cancel booking
BookingRouter.patch('/cancelbooking', async (req, res) => {
    try {
        const updates = {
            status: 'Cancelled',
            payment: 'Cancelled'
        }
        const booking = await BookingModel.findByIdAndUpdate(req.body.id, updates, { new: true });
        let schedule = await ScheduleModel.findById(req.body.schedule);
        let updatedSchedule = await ScheduleModel.findByIdAndUpdate(req.body.schedule, { capacity: schedule.capacity + 1 }, { new: true });
        res.send(booking)
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})

//delete booking
BookingRouter.delete('/deletebooking', async (req, res) => {
    try {
        const booking = await BookingModel.findByIdAndDelete(req.body.id);
        res.send(booking)
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})

//update booking
BookingRouter.patch('/updatebooking', async (req, res) => {
    try {
        const updates = {
            payment: req.body.payment,
            amount: req.body.amount
        }
        let booking = await BookingModel.findByIdAndUpdate(req.body.id, updates, { new: true });
        res.send(booking)
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})

module.exports = BookingRouter;