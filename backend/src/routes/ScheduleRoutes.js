const express = require('express')

const ScheduleRouter = express.Router();

const ScheduleModel = require('../models/Schedule');

//add schedule
ScheduleRouter.post('/addschedule', async (req, res) => {
    try {
        let schedule = ScheduleModel({
            fishpond: req.body.fishpond,
            startDate: req.body.startDate,
            endDate: req.body.endDate,
            capacity: req.body.capacity,
            amount: req.body.amount
        })
        schedule = await schedule.save()
        res.send(schedule);
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})


//get all schedule
ScheduleRouter.get('/schedules', async (req, res) => {
    try {
        const schedules = await ScheduleModel.find().populate({
            path: 'fishpond',
            populate: {
                path: 'user',
            }
        });
        res.send(schedules);
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})

//delete schedule
ScheduleRouter.delete('/deleteschedule', async (req, res) => {
    try {
        const schedule = await ScheduleModel.findByIdAndDelete(req.body.id);
        res.send(schedule);
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})

//update schedule
ScheduleRouter.patch('/updateschedule', async (req, res) => {
    // console.log(req.body)
    try {
        const updates = {
            fishpond: req.body.fishpond,
            startDate: req.body.startDate,
            endDate: req.body.endDate,
            capacity: req.body.capacity,
            amount: req.body.amount
        }
        let schedule = await ScheduleModel.findByIdAndUpdate(req.body.id, updates, { new: true }).populate({
            path: 'fishpond',
            populate: {
                path: 'user',
            }
        });
        res.send(schedule);
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})

ScheduleRouter.patch('/updateopen', async (req, res) => {
    try {
        let update = { isOpen: req.body.isOpen }
        let schedule = await ScheduleModel.findByIdAndUpdate(req.body.id, update, { new: true })
        res.send(schedule);
    } catch (e) {
        res.status(401).send('Bad Request. Please try again');
    }
})


module.exports = ScheduleRouter;