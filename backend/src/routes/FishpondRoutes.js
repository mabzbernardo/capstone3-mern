const express = require('express')

const FishpondRouter = express.Router();

const FishpondModel = require('../models/Fishpond');

// const fs = require('fs');

//add fishpond 
FishpondRouter.post('/addfishpond', async (req, res) => {
    // console.log(req.body)
    try {
        let fishpond = FishpondModel({
            fishpondname: req.body.fishpondname,
            user: req.body.user,
            image: req.body.image,
            description: req.body.description,
            capacity: req.body.capacity
        })
        fishpond = await fishpond.save();
        res.send(fishpond);

    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})

//get all fishpond
FishpondRouter.get('/fishponds', async (req, res) => {
    try {
        const fishponds = await FishpondModel.find().populate(['user']);
        res.send(fishponds);
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})

//delete fishpond
FishpondRouter.delete('/deletefishpond', async (req, res) => {
    try {
        const fishpond = await FishpondModel.findByIdAndDelete(req.body.id);
        // await fs.unlinkSync("public/" + fishpond.image)
        res.send(fishpond);
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})

//update fishpond
FishpondRouter.patch('/updatefishpond', async (req, res) => {
    // console.log(req.body)
    try {
        const updates = {
            fishpondname: req.body.fishpondname,
            image: req.body.image,
            description: req.body.description,
            capacity: req.body.capacity
        }
        let fishpond = await FishpondModel.findByIdAndUpdate(req.body.id, updates, { new: true });
        res.send(fishpond);
    } catch (e) {
        res.status(401).send('Bad request. Please try again.')
    }
})


module.exports = FishpondRouter;