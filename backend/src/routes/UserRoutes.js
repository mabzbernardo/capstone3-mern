const express = require('express');

const UserRouter = express.Router();

const UserModel = require('../models/User');

const bcrypt = require('bcryptjs');

const jwt = require('jsonwebtoken');

const config = require('../config');

UserRouter.post('/register', async (req, res) => {
    //validation

    //create a user
    let user = UserModel({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
    });

    const salt = bcrypt.genSaltSync(10);
    const hashed = bcrypt.hashSync(req.body.password, salt);

    user.password = hashed;
    try {
        user = await user.save();
        res.send(user);
    } catch (e) {
        res.status(401).send('Bad Request. Please try again')
    }
})

//view all users
UserRouter.get('/users', async (req, res) => {
    try {
        const users = await UserModel.find();
        res.send(users);
    } catch (e) {
        res.status(404).send("Error Occurred. Please try again.");
    }
})

//login

UserRouter.post('/login', async (req, res) => {

    let user = await UserModel.findOne({ email: req.body.email });

    if (!user) return res.status(401).send('Invalid Email');

    const matched = await bcrypt.compare(req.body.password, user.password);

    if (!matched) return res.status(401).send('Incorrect Password');

    const token = jwt.sign({
        email: user.email,
        id: user._id,
        isAdmin: user.isAdmin
    }, config.secret, { expiresIn: 1800000 })

    const loggedInUser = {
        token: token,
        user: {
            firstName: user.firstName,
            lastName: user.lastName,
            id: user._id,
            isAdmin: user.isAdmin,
            email: user.email
        }
    };

    res.header('x-auth-token', token).send(loggedInUser);
})

module.exports = UserRouter;