const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FishpondSchema = new Schema({
    fishpondname: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    image: String,
    description: String,
    capacity: Number,
    isOpen: {
        type: Boolean,
        default: false
    },
    isClosed: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('Fishpond', FishpondSchema);
