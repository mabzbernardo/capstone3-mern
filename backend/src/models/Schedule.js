const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ScheduleSchema = new Schema({
    fishpond: {
        type: Schema.Types.ObjectId,
        ref: 'Fishpond'
    },
    startDate: String,
    endDate: String,
    capacity: Number,
    amount: Number,
    isOpen: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('Schedule', ScheduleSchema);