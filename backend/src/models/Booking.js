const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const BookingSchema = new Schema({
    schedule: {
        type: Schema.Types.ObjectId,
        ref: 'Schedule'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    fishpond: {
        type: Schema.Types.ObjectId,
        ref: 'Fishpond'
    },
    status: {
        type: String,
        default: "Pending"
    },
    payment: String,
    amount: Number
}, {
    timestamps: true
})

module.exports = mongoose.model('Booking', BookingSchema)