const express = require('express');

const app = express();

const cors = require('cors');

const mongoose = require('mongoose')

const config = require('./src/config');

//call multer
const multer = require('multer');

//call path
const path = require('path');

// allow saving files in the public directory
app.use(express.static(path.join(__dirname, 'public')));


//parsing data from url
app.use(express.urlencoded({ extended: false }));

//parsing data from a form
app.use(express.json());

//connect to database
mongoose.connect(config.databaseURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(() => {
    console.log("Remote Database Connection Successful")
})

app.use(cors());

const PORT = config.PORT;

app.listen(PORT, () => {
    console.log(`Listening on PORT ${PORT}`)
})


//User API
const userRoutes = require('./src/routes/UserRoutes')
app.use('/', userRoutes);

//Fishpond API
const fishpondRoutes = require('./src/routes/FishpondRoutes');
app.use('/admin', fishpondRoutes);

//Schedule API
const scheduleRoutes = require('./src/routes/ScheduleRoutes');
app.use('/admin', scheduleRoutes);

//Booking API
const bookingRoutes = require('./src/routes/BookingRoutes');
app.use('/admin', bookingRoutes);

//Payment API
const paymentRoutes = require('./src/routes/PaymentRoutes');
app.use('/', paymentRoutes);


//for uploading of images
// define where to store
let storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, 'public/images/uploads');
    },
    filename: (req, file, callback) => {
        callback(null, Date.now() + '-' + file.originalname);
    }
});

const upload = multer({ storage });

app.post('/upload', upload.single('image'), (req, res) => {
    if (req.file) {
        res.json({ imageUrl: `images/uploads/${req.file.filename}` })
    } else {
        res.status(401).send('Bad Request. Please try again later.');
    }
});